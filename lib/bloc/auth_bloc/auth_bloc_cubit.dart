import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/common/common.dart';
import 'package:majootestcase/services/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async {
    emit(AuthBlocLoadingState());
    await Future.delayed(
      const Duration(
        seconds: 2,
      ),
    );

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    /// Check auth user data
    try {
      List<Map<String, dynamic>> rawAllUser = await SQLHelper.getUsers();

      if (rawAllUser.isNotEmpty) {
        for (var element in rawAllUser) {
          if (user.email == element['email'] &&
              user.password == element['password']) {
            await sharedPreferences.setBool(
              "is_logged_in",
              true,
            );
            String data = user.toJson().toString();
            sharedPreferences.setString(
              "user_value",
              data,
            );
          } else {
            return emit(AuthBlocErrorState(
                "Login gagal, periksa kembali inputan anda"));
          }
        }
      } else {
        return emit(AuthBlocErrorState(
            "Login gagal, silahkan register terlebih dahulu"));
      }
    } catch (e) {
      return emit(AuthBlocErrorState("Cannot log in"));
    }

    emit(AuthBlocLoggedInState());
  }

  void registerUser(User user) async {
    emit(AuthBlocLoadingState());
    await Future.delayed(
      const Duration(
        seconds: 2,
      ),
    );

    /// Add data to sqlite
    try {
      await SQLHelper.addUser(
        username: user.userName,
        email: user.email,
        password: user.password,
      );
    } catch (e) {
      return emit(AuthBlocErrorState("Cannot save to sqlite"));
    }

    /// All validate passed
    emit(AuthBlocSuccesState());
  }
}
