import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/common/common.dart';
import 'package:majootestcase/services/services.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData({
    String? query,
  }) async {
    emit(HomeBlocLoadingState());
    ApiServices apiServices = ApiServices();

    try {
      MovieResponse? movieResponse = await apiServices.getMovieList(
        query: query,
      );

      if (movieResponse == null) {
        emit(HomeBlocErrorState("Error Unknown"));
      } else {
        emit(HomeBlocLoadedState(movieResponse.data));
      }
    } catch (e) {
      return emit(HomeBlocErrorState(e.toString()));
    }
  }
}
