import 'package:flutter/material.dart';

class ContainerButton extends StatelessWidget {
  final String title;
  final VoidCallback? action;

  const ContainerButton({
    Key? key,
    required this.title,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => action != null ? action!.call() : null,
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 10.0,
        ),
        width: double.maxFinite,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            10.0,
          ),
          color: Colors.blue,
        ),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w800,
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
