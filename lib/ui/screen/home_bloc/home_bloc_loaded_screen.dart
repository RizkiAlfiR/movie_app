import 'package:flutter/material.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/common.dart';
import 'package:majootestcase/ui/ui.dart';
import 'package:provider/src/provider.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie App"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          bottom: 15.0,
        ),
        child: ListView(
          physics: const ClampingScrollPhysics(),
          shrinkWrap: true,
          children: [
            const SizedBox(
              height: 6.0,
            ),
            _buildSearchField(context),
            const SizedBox(
              height: 16.0,
            ),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: data!.length,
              itemBuilder: (context, index) {
                return ContainerMovieList(
                  action: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DetailMovieScreen(
                        singleData: data?[index],
                      ),
                    ),
                  ),
                  data: data![index],
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSearchField(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: TextFormField(
          onChanged: (value) {},
          onFieldSubmitted: (String? value) =>
              context.read<HomeBlocCubit>().fetchingData(
                    query: value,
                  ),
          decoration: InputDecoration(
            fillColor: Colors.white,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              ),
              borderRadius: BorderRadius.circular(15),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              ),
              borderRadius: BorderRadius.circular(15),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              ),
              borderRadius: BorderRadius.circular(15),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              ),
              borderRadius: BorderRadius.circular(15),
            ),
            hintText: "Type something",
            hintStyle: TextStyle(
              fontSize: 12.0,
              color: Colors.grey,
            ),
            prefixIcon: const Icon(
              Icons.search,
              color: Colors.grey,
            ),
          ),
        ),
      ),
    );
  }
}
