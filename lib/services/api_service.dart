import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/common/common.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieResponse?> getMovieList({
    String? query,
  }) async {
    try {
      var dio = await (dioConfig.dio(
        query: query,
      ));
      Response<String> response = await dio!.get("");

      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));

      return movieResponse;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
