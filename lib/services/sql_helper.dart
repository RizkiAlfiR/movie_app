import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE users(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        email TEXT,
        username TEXT,
        password TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'movie_app.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  /// Add new user
  static Future<int> addUser({
    String? email,
    String? username,
    String? password,
  }) async {
    final db = await SQLHelper.db();

    final data = {
      'email': email,
      'username': username,
      'password': password,
    };

    final id = await db.insert(
      'users',
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );

    return id;
  }

  /// Get all users
  static Future<List<Map<String, dynamic>>> getUsers() async {
    final db = await SQLHelper.db();

    return db.query(
      'users',
      orderBy: "id",
    );
  }

  static Future<List<Map<String, dynamic>>> getSingleUser(int id) async {
    final db = await SQLHelper.db();

    return db.query(
      'users',
      where: "id = ?",
      whereArgs: [id],
      limit: 1,
    );
  }

  /// Update an user data by id
  /// But not used in this project
  static Future<int> updateItem({
    required int id,
    required String email,
    required String username,
    required String password,
  }) async {
    final db = await SQLHelper.db();

    final data = {
      'email': email,
      'username': username,
      'password': password,
      'createdAt': DateTime.now().toString()
    };

    final result = await db.update(
      'users',
      data,
      where: "id = ?",
      whereArgs: [id],
    );

    return result;
  }

  /// Delete an user data by id
  /// But not used in this project
  static Future<void> deleteItem(int id) async {
    final db = await SQLHelper.db();

    try {
      await db.delete(
        "users",
        where: "id = ?",
        whereArgs: [id],
      );
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
  }
}
